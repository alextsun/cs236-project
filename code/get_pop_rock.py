import youtube_dl
import matplotlib.pyplot as plt
from scipy import signal
from scipy.io import wavfile
import numpy as np
import os

fname = '../data/unbalanced_train_segments.csv'
pop_label = '/m/064t9'
rock_label = '/m/06by7'
classic_label = '/m/0ggq0m'
#pop_file = open('pop_ids.txt','w')
#rock_file = open('rock_ids.txt','w')
def download(s, genre):
    assert genre in ['pop', 'rock', 'classic']
    split = s.split(',')
    id = split[0]
    segment = int(float(split[1].strip()))
    secs = segment % 60
    mins = int(segment/60)
    ydl_opts = {
        'format':'worstaudio/worst',
        'ignoreerrors': True,
        'verbose':False,
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'wav',
            'preferredquality': '192'
        }],
        'postprocessor_args': ['-ss','0:{}:{}'.format(mins, secs), '-t', '0:0:10'],
        'prefer_ffmpeg':True,
        'verbose':False,
        'keepvideo':False,
        'outtmpl': '../data/{}/%(id)s.%(ext)s'.format(genre, id)
    }
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        dictMeta = ydl.extract_info('http://www.youtube.com/watch?v={}'.format(id), download=False)
        if dictMeta is None or dictMeta['duration'] > 480: # 8 min cutoff
            return 0
        ydl.download(['http://www.youtube.com/watch?v={}'.format(id)])

    file_name = '../data/{}/{}'.format(genre, id)
    if not os.path.exists(file_name + '.wav'):
        return 0 # did not download video
    #sample_rate, samples = wavfile.read(file_name + '.wav')
    #_, _, spectrogram = signal.spectrogram(samples, sample_rate)
    #np.save(file_name, spectrogram)
    return 1

samples_to_get = 500 # float("inf")
pop_samples = 0
rock_samples = 0
with open(fname,'r') as f:
    while True:
        if pop_samples >= samples_to_get and rock_samples >= samples_to_get:
            break
        s = f.readline()

        if not s:
            break
        if classic_label in s and rock_label not in s and pop_label not in s:
            download(s, 'classic')
        if pop_label in s and rock_label not in s and pop_samples < samples_to_get:
            pop_samples += download(s, 'pop')
        #if rock_label in s and pop_label not in s and rock_samples < samples_to_get:
        #    rock_samples += download(s, 'rock')

#pop_file.close()
#rock_file.close()

# python3 -m youtube_dl --restrict-filenames --ignore-errors -x --audio-format mp3 --postprocessor-args "-ss 0:0:10 -to 0:0:20" getvideo -a pop_ids.txt
