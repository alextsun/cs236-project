import torch
from models import Discriminator
from models import LSTMGenerator
import utils
import os
import librosa
import numpy as np
import librosa.display
from torch.utils.data import DataLoader
from data import SongDataset
import sounddevice as sd
import matplotlib.pyplot as plt
import librosa
import utils
import torch

netG_A2B = LSTMGenerator(SAMPLE_HEIGHT)
netG_B2A = LSTMGenerator(SAMPLE_HEIGHT)
netD_A = Discriminator(opt.input_nc)
netD_B = Discriminator(opt.output_nc)

netG_A2B.load_state_dict(torch.load('output/netG_A2B.pth'))
netG_B2A.load_state_dict(torch.load('output/netG_B2A.pth'))
netD_A.load_state_dict(torch.load('output/netD_A.pth'))
netD_B.load_state_dict(torch.load('output/netD_B.pth'))

if opt.cuda:
    netG_A2B.cuda()
    netG_B2A.cuda()
    netD_A.cuda()
    netD_B.cuda()

ind = 5
pop_dir = '../data/pop/'

ids = os.listdir(pop_dir)

signal = utils.load_wav(pop_dir + ids[ind])

spec, spec_min, spec_max  = utils.signal_to_spec_full(signal, nopad = True, db = True)
spec = spec[:,:2**int(np.log2(spec.shape[-1]))]
#spec_torch = torch.tensor(spec).unsqueeze(0)
spec_torch = torch.tensor(spec).unsqueeze(0).unsqueeze(0)
print(spec_torch.shape)
rock_spec = model_pop2rock(spec_torch)

rock_spec = (rock_spec + 1) / 2 * (spec_max - spec_min) + spec_min
spec = (spec + 1) / 2 * (spec_max - spec_min) + spec_min
