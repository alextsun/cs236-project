import argparse
import itertools

import torchvision.transforms as transforms
from torch.utils.data import DataLoader
from torch.autograd import Variable
from PIL import Image
import torch

from models import LSTMGenerator
from models import Discriminator
from models import SiameseNet
from utils import ReplayBuffer
from utils import LambdaLR
from utils import Logger
from utils import weights_init_normal
from utils import SAMPLE_HEIGHT, SAMPLE_WIDTH, WINDOW_WIDTH
from data import SongDataset
from random import random
from tqdm import tqdm

parser = argparse.ArgumentParser()
parser.add_argument('--epoch', type=int, default=0, help='starting epoch')
parser.add_argument('--n_epochs', type=int, default=400, help='number of epochs of training')
parser.add_argument('--batchSize', type=int, default=1, help='size of the batches')
parser.add_argument('--dataroot', type=str, default='../data', help='root directory of the dataset')
parser.add_argument('--lr', type=float, default=0.0001, help='initial learning rate')
parser.add_argument('--decay_epoch', type=int, default=100, help='epoch to start linearly decaying the learning rate to 0')
parser.add_argument('--input_nc', type=int, default=1, help='number of channels of input data')
parser.add_argument('--output_nc', type=int, default=1, help='number of channels of output data')
parser.add_argument('--cuda', action='store_true', help='use GPU computation')
parser.add_argument('--n_cpu', type=int, default=4, help='number of cpu threads to use during batch generation')
parser.add_argument('--gp', type=int, default=10, help='gradient penalty mult')
parser.add_argument('--enc_nc', type=int, default=16, help='siamese encoding dimension')
parser.add_argument('--delta_margin', type=int, default=1, help='siamese margin')
parser.add_argument('--lambda_travel', type=int, default=10, help='travel loss coeff')
parser.add_argument('--lambda_margin', type=int, default=2, help='margin loss coeff')
parser.add_argument('--lambda_identity', type=int, default=5, help='identity loss coeff')

opt = parser.parse_args()
#print(opt)

if torch.cuda.is_available() and not opt.cuda:
    print("WARNING: You have a CUDA device, so you should probably run with --cuda")

###### Definition of variables ######
# Networks
netG_A2B = LSTMGenerator(SAMPLE_HEIGHT)
netG_B2A = LSTMGenerator(SAMPLE_HEIGHT)
netD_A = Discriminator(opt.input_nc)
netD_B = Discriminator(opt.output_nc)

# Load checkpoints
# netG_A2B.load_state_dict(torch.load('output/netG_A2B.pth'))
# netG_B2A.load_state_dict(torch.load('output/netG_B2A.pth'))
# netD_A.load_state_dict(torch.load('output/netD_A.pth'))
# netD_B.load_state_dict(torch.load('output/netD_B.pth'))

if opt.cuda:
    netG_A2B.cuda()
    netG_B2A.cuda()
    netD_A.cuda()
    netD_B.cuda()

netG_A2B.apply(weights_init_normal)
netG_B2A.apply(weights_init_normal)
netD_A.apply(weights_init_normal)
netD_B.apply(weights_init_normal)
# netS_A.apply(weights_init_normal)
# netS_B.apply(weights_init_normal)

# Lossess
criterion_GAN = torch.nn.MSELoss()
criterion_cycle = torch.nn.L1Loss()
criterion_identity = torch.nn.L1Loss()
cosine_sim = torch.nn.CosineSimilarity()

# Optimizers & LR schedulers
optimizer_G = torch.optim.Adam(itertools.chain(netG_A2B.parameters(), netG_B2A.parameters()),
                                lr=opt.lr, betas=(0.5, 0.999))
optimizer_D_A = torch.optim.Adam(netD_A.parameters(), lr=opt.lr/10, betas=(0.5, 0.999))
optimizer_D_B = torch.optim.Adam(netD_B.parameters(), lr=opt.lr/10, betas=(0.5, 0.999))



lr_scheduler_G = torch.optim.lr_scheduler.LambdaLR(optimizer_G, lr_lambda=LambdaLR(opt.n_epochs, opt.epoch, opt.decay_epoch).step)
lr_scheduler_D_A = torch.optim.lr_scheduler.LambdaLR(optimizer_D_A, lr_lambda=LambdaLR(opt.n_epochs, opt.epoch, opt.decay_epoch).step)
lr_scheduler_D_B = torch.optim.lr_scheduler.LambdaLR(optimizer_D_B, lr_lambda=LambdaLR(opt.n_epochs, opt.epoch, opt.decay_epoch).step)

# Inputs & targets memory allocation
Tensor = torch.cuda.FloatTensor if opt.cuda else torch.Tensor
input_A = Tensor(opt.batchSize, opt.input_nc, SAMPLE_HEIGHT, WINDOW_WIDTH)
input_B = Tensor(opt.batchSize, opt.output_nc, SAMPLE_HEIGHT, WINDOW_WIDTH)

# Flip labels (original was real=1.0, fake = 0.0)
target_real = Variable(Tensor(opt.batchSize).fill_(0.0), requires_grad=False)
target_fake = Variable(Tensor(opt.batchSize).fill_(1.0), requires_grad=False)

fake_A_buffer = ReplayBuffer()
fake_B_buffer = ReplayBuffer()

d1 = 'pop'
d2 = 'rock'
# Dataset loader
data = SongDataset(opt.dataroot, d1 = d1, d2 = d2, db = True)
#print(len(data))
dataloader = DataLoader(data,    batch_size=opt.batchSize, shuffle=True, num_workers=opt.n_cpu)

# Loss plot
logger = Logger(opt.n_epochs, len(dataloader), start_epoch = opt.epoch)
###################################

def travel_loss(t1, t2):
    return (1- cosine_sim(t_orig, t_trans)) + torch.norm(t_orig - t_trans, dim = -1)

DISC_SKIP = 5

def main():
    ###### Training ######
    for epoch in range(opt.epoch, opt.n_epochs):
        for i, batch in enumerate(dataloader):
            
            # Make random soft labels (real = (0.0, 0.1), fake = (0.9, 1.0))
            target_real[0] = random()*0.1
            target_fake[0] = random()*0.1 + 0.9
            
            length_1 = len(batch[d1])
            length_2 = len(batch[d2])

            disc_counter = 0

            for j in tqdm(range(max(length_1, length_2))):
                # Set model input
                real_A = Variable(input_A.copy_(batch[d1][j % length_1]))
                real_B = Variable(input_B.copy_(batch[d2][j % length_2]))

                ###### Generators A2B and B2A ######
                optimizer_G.zero_grad()

                # Identity loss
                # G_A2B(B) should equal B if real B is fed
                same_B = netG_A2B(real_B)
                loss_identity_B = criterion_identity(same_B, real_B)*10.0
                # G_B2A(A) should equal A if real A is fed
                same_A = netG_B2A(real_A)
                loss_identity_A = criterion_identity(same_A, real_A)*10.0

                # GAN loss (why isnt this higher?)
                fake_B = netG_A2B(real_A)
                pred_fake = netD_B(fake_B)
                loss_GAN_A2B = criterion_GAN(pred_fake, target_real)

                fake_A = netG_B2A(real_B)
                pred_fake = netD_A(fake_A)
                loss_GAN_B2A = criterion_GAN(pred_fake, target_real)

                # Cycle loss
                recovered_A = netG_B2A(fake_B)
                loss_cycle_ABA = criterion_cycle(recovered_A, real_A)*50.0

                recovered_B = netG_A2B(fake_A)
                loss_cycle_BAB = criterion_cycle(recovered_B, real_B)*50.0

                # Total loss
                #loss_G = loss_identity_A + loss_identity_B + loss_GAN_A2B + loss_GAN_B2A + loss_cycle_ABA + loss_cycle_BAB
                loss_G =  loss_GAN_A2B + loss_GAN_B2A + loss_cycle_ABA + loss_cycle_BAB + loss_identity_A + loss_identity_B
                loss_G.backward()
                
                optimizer_G.step()
                ###################################
                # Step the discriminator once for every DISC_SKIP steps

                if disc_counter == DISC_SKIP:
                    disc_counter = 0
                else:
                    disc_counter += 1
                    continue
                ###### Discriminator A ######
                optimizer_D_A.zero_grad()

                # Real loss
                pred_real = netD_A(real_A)
                loss_D_real = criterion_GAN(pred_real, target_real)

                # Fake loss
                fake_A = fake_A_buffer.push_and_pop(fake_A)
                pred_fake = netD_A(fake_A.detach())
                loss_D_fake = criterion_GAN(pred_fake, target_fake)

                # Gradient penalty
                # Take r_A as convex comb of real_A, fake_A
                alpha = Tensor(1).uniform_()
                r_A = alpha * real_A + (1-alpha)* fake_A
                r_A.requires_grad_(True)
                D_r_A = netD_A(r_A)
                gp_A = (torch.norm(torch.autograd.grad(D_r_A, r_A, create_graph = True, retain_graph = True)[0] )-1)**2
                 

                # Total loss
                loss_D_A = (loss_D_real + loss_D_fake)*0.5 + opt.gp * gp_A
                loss_D_A.backward(retain_graph=True)


                optimizer_D_A.step()
                ###################################

                ###### Discriminator B ######
                optimizer_D_B.zero_grad()

                # Real loss
                pred_real = netD_B(real_B)
                loss_D_real = criterion_GAN(pred_real, target_real)
                
                # Fake loss
                fake_B = fake_B_buffer.push_and_pop(fake_B)
                pred_fake = netD_B(fake_B.detach())
                loss_D_fake = criterion_GAN(pred_fake, target_fake)

                # Gradient penalty
                # Take r_A as convex comb of real_A, fake_A
                alpha = Tensor(1).uniform_()
                r_B = alpha * real_B + (1-alpha)* fake_B
                r_B.requires_grad_(True)
                D_r_B = netD_B(r_B)
                gp_B = (torch.norm(torch.autograd.grad(D_r_B, r_B,  create_graph = True, retain_graph = True)[0] )-1)**2
                 

                # Total loss
                loss_D_B = (loss_D_real + loss_D_fake)*0.5 + opt.gp * gp_B
                loss_D_B.backward(retain_graph = True)

                optimizer_D_B.step()
                ###################################


                # Progress report (http://localhost:8097)
            logger.log({'loss_G': loss_G, 'loss_G_GAN': (loss_GAN_A2B + loss_GAN_B2A),
                        'loss_G_cycle': (loss_cycle_ABA + loss_cycle_BAB), 'loss_D': (loss_D_A + loss_D_B)}, 
                        images={'real_A': real_A, 'real_B': real_B, 'fake_A': fake_A, 'fake_B': fake_B})
            #logger.log({'loss_G': loss_G,  'loss_G_GAN': (loss_GAN_A2B + loss_GAN_B2A),
            #            'loss_G_cycle': (loss_cycle_ABA + loss_cycle_BAB), 'loss_D': (loss_D_A + loss_D_B)}, 
            #            images={'real_A': real_A, 'real_B': real_B, 'fake_A': fake_A, 'fake_B': fake_B})

            

        lr_scheduler_G.step()
        lr_scheduler_D_A.step()
        lr_scheduler_D_B.step()

        # Save models checkpoints
        torch.save(netG_A2B.state_dict(), 'output/netG_A2B.pth')
        torch.save(netG_B2A.state_dict(), 'output/netG_B2A.pth')
        torch.save(netD_A.state_dict(), 'output/netD_A.pth')
        torch.save(netD_B.state_dict(), 'output/netD_B.pth')
    ###################################

if __name__ == '__main__':
    main()
