import torch.nn as nn
import torch.nn.functional as F
import torch
from torch.nn.utils import spectral_norm



class ResidualBlock(nn.Module):
    def __init__(self, in_features):
        super(ResidualBlock, self).__init__()

        conv_block = [  nn.ReflectionPad2d(1),
                        nn.Conv2d(in_features, in_features, 3),
                        nn.InstanceNorm2d(in_features),
                        nn.ReLU(),
                        nn.ReflectionPad2d(1),
                        nn.Conv2d(in_features, in_features, 3),
                        nn.InstanceNorm2d(in_features)  ]

        self.conv_block = nn.Sequential(*conv_block)
        self.relu = nn.ReLU()

    def forward(self, x):
        return self.relu(x + self.conv_block(x))

class LSTMGenerator(nn.Module):
    def __init__(self, input_size, num_layers = 2, dropout = 0.9):
        super(LSTMGenerator, self).__init__()
        self.hidden_size = input_size
        self.num_layers = num_layers

        # set up modules for recurrent neural networks
        self.rnn = nn.LSTM(input_size = input_size,
                           hidden_size = input_size,
                           num_layers = num_layers,
                           batch_first = True,
                           dropout = dropout,
                           bidirectional = False)
        # self.rnn.apply(weights_init)

        # set up modules to compute classification
        #self.classifier = nn.Linear(hidden_size * 2, num_classes)
        #self.classifier.apply(weights_init) 

    def forward(self, x):
        # For db
        return torch.transpose(self.rnn(torch.transpose(x, 2, 3).squeeze().unsqueeze(0))[0].unsqueeze(1), 2, 3)

class Generator(nn.Module):
    def __init__(self, input_nc, output_nc, n_residual_blocks=9):
        super(Generator, self).__init__()

        # Initial convolution block       
        model = [   nn.ReflectionPad2d(3),
                    nn.Conv2d(input_nc, 64, 7),
                    nn.InstanceNorm2d(64),
                    nn.ReLU() ]

        # Downsampling
        in_features = 64
        out_features = in_features*2
        for _ in range(2):
            model += [  
                        nn.Conv2d(in_features, out_features, 3, padding=1, padding_mode = 'reflect'),
                        nn.MaxPool2d(2, stride=2),
                        nn.InstanceNorm2d(out_features),
                        nn.ReLU() ]
            in_features = out_features
            out_features = in_features*2

        # Residual blocks
        for _ in range(n_residual_blocks):
            model += [ResidualBlock(in_features)]

        # Upsampling
        out_features = in_features//2
        for _ in range(2):
            # Replace transpose conv with nn interp + conv
            model += [  nn.Upsample(scale_factor = 2, mode = 'nearest'),
                        nn.Conv2d(in_features, out_features, 3, padding = 1, padding_mode = 'reflect'),
                        #nn.ConvTranspose2d(in_features, out_features, 3, stride=2, padding=1, output_padding=1),
                        nn.InstanceNorm2d(out_features),
                        nn.ReLU() ]
            in_features = out_features
            out_features = in_features//2

        # Output layer
        model += [  nn.ReflectionPad2d(3),
                    nn.Conv2d(64, output_nc, 7),
                    nn.InstanceNorm2d(output_nc),
                    nn.Tanh()]

        self.model = nn.Sequential(*model)

    def forward(self, x):
        # For db
        return self.model(x)
        #return F.relu(self.model(x))
        

class Discriminator(nn.Module):
    def __init__(self, input_nc):
        super(Discriminator, self).__init__()

        # A bunch of convolutions one after another
        model = [   nn.Conv2d(input_nc, 64, 4, stride=2, padding=1, padding_mode = 'reflect'),
                    nn.InstanceNorm2d(64), 
                    nn.LeakyReLU(0.2) ]

        model += [  nn.Conv2d(64, 128, 4, stride=2, padding=1, padding_mode = 'reflect'),
                    nn.InstanceNorm2d(128), 
                    nn.LeakyReLU(0.2) ]

        model += [  nn.Conv2d(128, 256, 4, stride=2, padding=1, padding_mode = 'reflect'),
                    nn.InstanceNorm2d(256), 
                    nn.LeakyReLU(0.2) ]

        model += [  nn.Conv2d(256, 512, 4, padding=1, padding_mode = 'reflect'),
                    nn.InstanceNorm2d(512), 
                    nn.LeakyReLU(0.2) ]

                    
        # FCN classification layer
        model += [nn.Conv2d(512, 1, 4, padding=1, padding_mode = 'reflect')]

        self.model = nn.Sequential(*model)

    def forward(self, x):
        x =  self.model(x)
        # Average pooling and flatten
        return torch.sigmoid(F.avg_pool2d(x, x.size()[2:]).view(x.size()[0], -1))


class UnetDSStage(nn.Module):
    def __init__(self, input_nc, output_nc):
        super(UnetDSStage, self).__init__()

        model = [
            nn.Conv2d(input_nc, output_nc, kernel_size = 3, padding = 1),
            nn.InstanceNorm2d(output_nc),
            nn.ReLU(),
            nn.Conv2d(output_nc, output_nc, kernel_size = 3, padding = 1),
            nn.InstanceNorm2d(output_nc),
            nn.ReLU(),
            nn.Conv2d(output_nc, output_nc, kernel_size = 3, padding = 1),
            nn.InstanceNorm2d(output_nc),
            nn.ReLU()
        ]
        self.model = nn.Sequential(*model)
        self.down = nn.MaxPool2d(2,stride = 2)

    def forward(self, input_tensor):
        model_out = self.model(input_tensor)

        return self.down(model_out), model_out
        
class UnetUSStage(nn.Module):
    def __init__(self, input_nc, output_nc):
        super(UnetUSStage, self).__init__()

        model = [
            nn.Conv2d(input_nc, output_nc, kernel_size = 3, padding = 1),
            nn.InstanceNorm2d(output_nc),
            nn.ReLU(),
            nn.Conv2d(output_nc, output_nc, kernel_size = 3, padding = 1),
            nn.InstanceNorm2d(output_nc),
            nn.ReLU(),
            nn.Conv2d(output_nc, output_nc, kernel_size = 3, padding = 1),
            nn.InstanceNorm2d(output_nc),
            nn.ReLU()
        ]
        self.us = nn.Upsample(scale_factor = 2 , mode = 'nearest')
        self.model = nn.Sequential(*model)

    def forward(self, input_tensor, input_feature):
        # concatenate along feature dimension
        cat = torch.cat([self.us(input_tensor), input_feature], dim = 1)
        return self.model(cat)
        
class UnetGenerator(nn.Module):
    def __init__(self, input_nc, output_nc, n_stages = 4, channel_init = 64):
        super(UnetGenerator, self).__init__()
        self.ds = []
        self.us = []
        nc = input_nc
        self.n_stages = n_stages
        assert output_nc == input_nc
        next_nc = channel_init
        for i in range(self.n_stages):
            self.ds.append(UnetDSStage(nc, next_nc))
            self.us.append(UnetUSStage(next_nc*2, nc))
            nc = next_nc
            next_nc *= 2
        self.ds = nn.ModuleList(self.ds)
        self.us = nn.ModuleList(self.us)
        self.final_conv = nn.Conv2d(input_nc, output_nc, 1)
        self.final_norm = nn.InstanceNorm2d(output_nc)
        self.final_act = nn.Tanh()
        
    def forward(self, x):

        features = []
        for i in range(self.n_stages):
            x, feature = self.ds[i](x)
            features.append(feature)
        
        for i in range(self.n_stages):
            ind = -1 -i
            x = self.us[ind](x, features[ind])
        
        return self.final_act(self.final_norm(self.final_conv(x)))



class ResidualBlock1d(nn.Module):
    def __init__(self, in_features):
        super(ResidualBlock1d, self).__init__()

        conv_block = [  nn.ReflectionPad1d(2),
                        nn.Conv1d(in_features, in_features, 5),
                        nn.InstanceNorm1d(in_features),
                        nn.ReLU(),
                        nn.ReflectionPad1d(2),
                        nn.Conv1d(in_features, in_features, 5),
                        nn.InstanceNorm1d(in_features)  ]

        self.conv_block = nn.Sequential(*conv_block)

    def forward(self, x):
        return x + self.conv_block(x)


class Generator1d(nn.Module):
    def __init__(self, input_nc, output_nc, n_residual_blocks=9, init_channel = 64):
        super(Generator1d, self).__init__()

        # Initial convolution block       
        model = [   nn.ReflectionPad1d(3),
                    nn.Conv1d(input_nc, init_channel, 7),
                    nn.InstanceNorm1d(init_channel),
                    nn.ReLU() ]

        # Downsampling
        in_features = init_channel
        out_features = in_features//2
        for _ in range(2):
            model += [  nn.Conv1d(in_features, out_features, 3, padding=1),
                        nn.InstanceNorm1d(out_features),
                        nn.ReLU() ]
            in_features = out_features
            out_features = in_features

        model += [nn.Dropout2d(0.2)]

        # Residual blocks
        for _ in range(n_residual_blocks):
            model += [ResidualBlock1d(in_features)]

        # Upsampling
        out_features = in_features*2
        for _ in range(2):
            # Replace transpose conv with nn interp + conv
            model += [  #nn.Upsample(scale_factor = 2, mode = 'nearest'),
                        nn.Conv1d(in_features, out_features, 3, padding = 1),
                        #nn.ConvTranspose2d(in_features, out_features, 3, stride=2, padding=1, output_padding=1),
                        nn.InstanceNorm1d(out_features),
                        nn.ReLU() ]
            in_features = out_features
            out_features = in_features
        
        model += [nn.Dropout2d(0.2)]
        # Output layer
        model += [  nn.ReflectionPad1d(3),
                    nn.Conv1d(init_channel, output_nc, 7),
                    nn.Tanh()]

        self.model = nn.Sequential(*model)

    def forward(self, x):
        # For db
        return self.model(x)
        #return F.relu(self.model(x))
        

class Discriminator1d(nn.Module):
    def __init__(self, input_nc):
        super(Discriminator1d, self).__init__()

        # A bunch of convolutions one after another
        model = [   nn.Conv1d(input_nc, 128, 4, padding=1),
                    nn.LeakyReLU(0.2) ]

        model += [  nn.Conv1d(128, 256, 4, padding=1),
                    nn.InstanceNorm1d(256), 
                    nn.LeakyReLU(0.2) ]

        model += [  nn.Conv1d(256, 256, 4,  padding=1),
                    nn.InstanceNorm1d(126), 
                    nn.LeakyReLU(0.2) ]

        model += [  nn.Conv1d(256, 256, 4, padding=1),
                    nn.InstanceNorm1d(256), 
                    nn.LeakyReLU(0.2, inplace=True) ]

        # FCN classification layer
        model += [nn.Conv1d(256, 1, 4, padding=1)]

        self.model = nn.Sequential(*model)

    def forward(self, x):
        x =  self.model(x)
        # Average pooling and flatten
        return F.avg_pool1d(x, x.size()[2:]).view(x.size()[0], -1)

class Flatten(nn.Module):
    def forward(self, x):
        return x.view(x.size()[0], -1)

class SiameseSubnet(nn.Module):
    def __init__(self, input_nc, output_nc, blocks=5):
        super(SiameseSubnet, self).__init__()
        model = [   nn.Conv2d(input_nc, 128, 4, stride=2, padding=1, padding_mode = 'reflect'),
                    nn.LeakyReLU(0.2) ]

        model += [  nn.Conv2d(128, 256, 4, stride=2, padding=1, padding_mode = 'reflect'),
                    nn.InstanceNorm2d(256), 
                    nn.LeakyReLU(0.2) ]

        model += [  nn.Conv2d(256, 256, 4, stride=2, padding=1, padding_mode = 'reflect'),
                    nn.InstanceNorm2d(256), 
                    nn.LeakyReLU(0.2), ]

        #model += [  nn.Conv2d(128, 256, 4, padding=1),
        #            nn.InstanceNorm2d(256), 
        #            nn.LeakyReLU(0.2),
        #            nn.Dropout2d(0.6) ]

        model += [  nn.Conv2d(256, 512, 4, padding=1, padding_mode = 'reflect'),
                    nn.InstanceNorm2d(512), 
                    nn.LeakyReLU(0.2), ]
                    
        # FCN classification layer
        #model += [nn.Conv2d(512, output_nc, 4, padding=1)
        #          ]

        self.model = nn.Sequential(*model)
        self.linear1 = nn.Linear(512, output_nc)
        self.linear2 = nn.Linear(output_nc, output_nc)
        self.linear3 = nn.Linear(output_nc, output_nc)
    
    def forward(self, x):
        # Global average pooling, then a linear layer
        y = self.model(x)
        y = F.relu(self.linear1(F.avg_pool2d(y, y.size()[2:]).view(y.size()[0], -1)))
        y = F.relu(self.linear2(y))
        return self.linear3(y)

class SiameseNet(nn.Module):
    def __init__(self, input_nc, output_nc, blocks =5 ):
        super(SiameseNet, self).__init__()
        self.output_nc = output_nc
        self.subnet = SiameseSubnet(input_nc, output_nc, blocks)

    # Take in 2 data points
    def forward(self, x1, x2):
        out1 = self.subnet(x1)
        out2 = self.subnet(x2)
        return out1 - out2


class LSTMGenerator(nn.Module):
    def __init__(self, input_size, num_layers = 2, dropout = 0.9,batch_size = 1):
        super(LSTMGenerator, self).__init__()
        self.hidden_size = input_size
        self.batch_size = batch_size
        self.num_layers = num_layers

        # set up modules for recurrent neural networks
        self.rnn = nn.LSTM(input_size = input_size,
                           hidden_size = input_size,
                           num_layers = num_layers,
                           batch_first = True,
                           dropout = dropout,
                           bidirectional = False)
        # self.rnn.apply(weights_init)

        # set up modules to compute classification
        #self.classifier = nn.Linear(hidden_size * 2, num_classes)
        #self.classifier.apply(weights_init) 

    def forward(self, x):
        # For db
        y = torch.transpose(x, 2, 3).squeeze()
        if self.batch_size == 1:
            y = y.unsqueeze(0)
        return torch.transpose(self.rnn(y)[0].unsqueeze(1), 2, 3)
