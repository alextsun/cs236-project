import argparse
import itertools

import torchvision.transforms as transforms
from torch.utils.data import DataLoader
from torch.autograd import Variable
import torch.nn.functional as F
from PIL import Image
import torch

from models import Generator
from models import UnetGenerator
from models import LSTMGenerator
from models import Discriminator
from models import SiameseNet
from utils import ReplayBuffer
from utils import LambdaLR
from utils import Logger
from utils import weights_init_normal
from utils import SAMPLE_HEIGHT, SAMPLE_WIDTH, WINDOW_WIDTH_SEC
from data import SongDataset
from random import random
from tqdm import tqdm

parser = argparse.ArgumentParser()
parser.add_argument('--epoch', type=int, default=0, help='starting epoch')
parser.add_argument('--n_epochs', type=int, default=400, help='number of epochs of training')
parser.add_argument('--batchSize', type=int, default=8, help='size of the batches')
parser.add_argument('--dataroot', type=str, default='../data', help='root directory of the dataset')
parser.add_argument('--lr', type=float, default=0.0001, help='initial learning rate')
parser.add_argument('--decay_epoch', type=int, default=100, help='epoch to start linearly decaying the learning rate to 0')
parser.add_argument('--input_nc', type=int, default=1, help='number of channels of input data')
parser.add_argument('--output_nc', type=int, default=1, help='number of channels of output data')
parser.add_argument('--cuda', action='store_true', help='use GPU computation')
parser.add_argument('--n_cpu', type=int, default=4, help='number of cpu threads to use during batch generation')
parser.add_argument('--gp', type=int, default=10, help='gradient penalty mult')
parser.add_argument('--enc_nc', type=int, default=128, help='siamese encoding dimension')
parser.add_argument('--delta_margin', type=int, default=5, help='siamese margin')
parser.add_argument('--lambda_travel', type=int, default=10, help='travel loss coeff')
parser.add_argument('--lambda_margin', type=int, default=10, help='margin loss coeff')
parser.add_argument('--lambda_identity', type=int, default=3, help='identity loss coeff')

opt = parser.parse_args()
#print(opt)

if torch.cuda.is_available() and not opt.cuda:
    print("WARNING: You have a CUDA device, so you should probably run with --cuda")

###### Definition of variables ######
# Networks
#netG = UnetGenerator(opt.input_nc, opt.output_nc, channel_init = 64, n_stages = 5)
netG = Generator(1,1,n_residual_blocks = 12)
#netG = LSTMGenerator(SAMPLE_HEIGHT, num_layers = 3, dropout = 0.5)
netD = Discriminator(opt.input_nc)
netS = SiameseNet(opt.input_nc, opt.enc_nc)

# Load checkpoints
netG.load_state_dict(torch.load('output/netG_travel.pth'))
netD.load_state_dict(torch.load('output/netD_travel.pth'))
netS.load_state_dict(torch.load('output/netS_travel.pth'))

if opt.cuda:
    netG.cuda()
    netD.cuda()
    netS.cuda()

#netG.apply(weights_init_normal)
#netD.apply(weights_init_normal)
#netS.apply(weights_init_normal)

# Lossess
#criterion_GAN = torch.nn.MSELoss()
criterion_GAN = torch.nn.BCELoss()
criterion_cycle = torch.nn.L1Loss()
criterion_identity = torch.nn.L1Loss()
cosine_sim = torch.nn.CosineSimilarity()

# Optimizers & LR schedulers
optimizer_G = torch.optim.Adam(netG.parameters(), lr=opt.lr, betas=(0.5, 0.999), weight_decay = 0.01)
optimizer_S = torch.optim.Adam(netS.parameters(), lr=opt.lr/50, betas=(0.5, 0.999), weight_decay = 0.01)
optimizer_D = torch.optim.Adam(netD.parameters(), lr=opt.lr/10, betas=(0.5, 0.999), weight_decay = 0.01)

lr_scheduler_G = torch.optim.lr_scheduler.LambdaLR(optimizer_G, lr_lambda=LambdaLR(opt.n_epochs, opt.epoch, opt.decay_epoch).step)
lr_scheduler_D = torch.optim.lr_scheduler.LambdaLR(optimizer_D, lr_lambda=LambdaLR(opt.n_epochs, opt.epoch, opt.decay_epoch).step)
lr_scheduler_S = torch.optim.lr_scheduler.LambdaLR(optimizer_S, lr_lambda=LambdaLR(opt.n_epochs, opt.epoch, opt.decay_epoch).step)

# Inputs & targets memory allocation
secs_audio = 1
Tensor = torch.cuda.FloatTensor if opt.cuda else torch.Tensor
input_A = Tensor(opt.batchSize, opt.input_nc, SAMPLE_HEIGHT, WINDOW_WIDTH_SEC * secs_audio)
input_B = Tensor(opt.batchSize, opt.output_nc, SAMPLE_HEIGHT, WINDOW_WIDTH_SEC * secs_audio)

# Flip labels (original was real=1.0, fake = 0.0)
target_real = Variable(Tensor(opt.batchSize).fill_(0.0), requires_grad=False)
target_fake = Variable(Tensor(opt.batchSize).fill_(1.0), requires_grad=False)

fake_B_buffer = ReplayBuffer()

d1 = 'pop_gtzan'
d2 = 'classical_gtzan'
# Dataset loader
data = SongDataset(opt.dataroot, d1 = d1, d2 = d2, db = True,seconds = secs_audio)
#print(len(data))
dataloader = DataLoader(data, batch_size=opt.batchSize, shuffle=True, num_workers=opt.n_cpu)

# Loss plot
logger = Logger(opt.n_epochs, len(dataloader), start_epoch = opt.epoch)
###################################


DISC_SKIP = 5
D_STEPS_PER_G = 1

def main():
    ###### Training ######
    for epoch in range(opt.epoch, opt.n_epochs):
        for i, batch in enumerate(dataloader):
                     
            length_1 = len(batch[d1])
            length_2 = len(batch[d2])

            disc_counter = 0

            for j in tqdm(range(max(length_1, length_2))):
                # Set model input
                if batch[d1][j % length_1].shape[0] != opt.batchSize:
                    continue
                real_A = Variable(input_A.copy_(batch[d1][j % length_1].unsqueeze(1)))
                real_B = Variable(input_B.copy_(batch[d2][j % length_2].unsqueeze(1)))

            
                ###### Generators A2B and B2A ######
                optimizer_G.zero_grad()
                optimizer_S.zero_grad()

                width = real_A.shape[-1]
                # Identity loss
                # G_A2B(B) should equal B if real B is fed
                b1 = real_B[:,:,:,:width//2]
                loss_identity_B = criterion_identity(netG(b1), b1)*opt.lambda_identity

                # Split samples
                a1 = real_A[:,:,:,:width//2]
                a2 = real_A[:,:,:,width//2:]

                # Get encoding distance of samples before transform
                t_orig = netS(a1, a2)

                fake_b1 = netG(a1)
                fake_b2 = netG(a2)

                # Get encoding distance after transform
                t_trans = netS(fake_b1, fake_b2)
                # Compute cosine sim plus euclidean dist
                # 1- cosine sim puts it in range of (0,2) with 0 exactly similar
                L_travel = (1- cosine_sim(t_orig, t_trans)) + torch.norm(t_orig - t_trans, dim = -1)
                #L_travel =  cosine_sim(t_orig, t_trans) + torch.norm(t_orig - t_trans, dim = -1)
                L_travel = torch.mean(L_travel * opt.lambda_travel)
                
                # Concatenate samples back for use in discriminator
                fake_B = torch.cat([fake_b1, fake_b2], dim = -1)
                pred_fake = netD(fake_B)
                loss_GAN = torch.mean(criterion_GAN(pred_fake.squeeze(), target_real.squeeze()))

                # Constrain the norms to be the same
                #norm_loss = criterion_GAN(torch.norm(real_A.view(opt.batchSize, opt.input_nc, -1), dim = -1), torch.norm(fake_B.view(opt.batchSize, opt.input_nc, -1), dim = -1))

                # Margin loss for siamese net
                L_margin = F.relu(opt.delta_margin - torch.norm(t_orig)) + F.relu(opt.delta_margin - torch.norm(t_trans))
                L_margin = torch.mean(L_margin * opt.lambda_margin)


                # Total loss
                loss_G = loss_GAN + L_travel #+ loss_identity_B #+ norm_loss
                loss_S = L_travel + L_margin

                loss_G.backward(retain_graph = True)
                loss_S.backward()
                
                optimizer_G.step()
                optimizer_S.step()
                ###################################
                # Step the discriminator once for every DISC_SKIP steps

                if disc_counter == DISC_SKIP:
                    disc_counter = 0
                else:
                    disc_counter += 1
                    continue
                for i in range(D_STEPS_PER_G):
                    ###### Discriminator A ######
                    optimizer_D.zero_grad()

                    # Real loss
                    pred_real = netD(real_B)
                    loss_D_real = criterion_GAN(pred_real.squeeze(), target_real.squeeze())

                    # Fake loss
                    fake_B = fake_B_buffer.push_and_pop(fake_B)
                    pred_fake = netD(fake_B.detach())

                    loss_D_fake_1 = criterion_GAN(pred_fake.squeeze(), target_fake.squeeze())

                    pred_real_A = netD(real_A)
                    loss_D_fake_2 = criterion_GAN(pred_real_A.squeeze(), target_fake.squeeze())


                    # Gradient penalty
                    # Take r_A as convex comb of real_A, fake_A
                    #alpha = Tensor(1).uniform_()
                    #r = alpha * real_B + (1-alpha)* fake_B
                    #r.requires_grad_(True)
                    #D_r = netD(r)
                    #gp = (torch.norm(torch.autograd.grad(D_r, r, create_graph = True, retain_graph = True)[0] )-1)**2
                    
                    # Total loss
                    loss_D = torch.mean((loss_D_real + loss_D_fake_1 + loss_D_fake_2)*0.5 )
                    loss_D.backward()


                    optimizer_D.step()
                    ###################################
            logger.log({'loss_G': loss_G, 'loss_S': loss_S,  'loss_G_GAN': loss_GAN, 'loss_travel': L_travel, 'loss_margin': L_margin,  'loss_D': loss_D })

            

        lr_scheduler_G.step()
        lr_scheduler_D.step()
        lr_scheduler_S.step()

        # Save models checkpoints
        torch.save(netG.state_dict(), 'output/netG_travel.pth')
        torch.save(netD.state_dict(), 'output/netD_travel.pth')
        torch.save(netS.state_dict(), 'output/netS_travel.pth')
    ###################################

if __name__ == '__main__':
    main()