#import torch
#from torch.utils import data
#import utils
#import os
#
#class SongDataset(data.Dataset):
#    def __init__(self, song_dir, song_ids):
#        self.song_dir = song_dir
#        if self.song_dir[-1] != '/':
#            self.song_dir += '/'
#        self.song_ids = song_ids
#
#    def __len__(self):
#        return len(self.song_ids)
#    
#    # Let pop be 0 and rock be 1
#    def __getitem__(self, index):
#        return utils.signal_to_spec_db(utils.load_wav(self.song_dir + self.song_ids[index]))

import glob
import random
import os

from torch.utils.data import Dataset
import utils

class SongDataset(Dataset):
    def __init__(self, root, data_prop = 1, db = True, d1 = 'pop', d2 = 'rock', seconds = 1):

        self.d1 = d1
        self.d2 = d2
        self.seconds = seconds

        #print(os.path.join(root, 'pop'))
        self.files_1 = sorted(glob.glob(os.path.join(root, self.d1) + '/*.*'))
        #print(self.files_pop)

        self.files_2 = sorted(glob.glob(os.path.join(root, self.d2) + '/*.*'))
        #print(self.files_rock)
        self.data_prop = data_prop
        self.offset = 0
        self.db = db

    def __getitem__(self, index):
        if index == 0:
            offset_index = random.randint(0,self.__len__() - 1)
        offset_index = index + self.offset
        if self.db:
            item_1 = utils.signal_to_spec_db(utils.load_wav(self.files_1[index % len(self.files_1)]), seconds = self.seconds)
            item_2 = utils.signal_to_spec_db(utils.load_wav(self.files_2[offset_index % len(self.files_2)]), seconds = self.seconds)
        else:
            item_1 = utils.signal_to_spec(utils.load_wav(self.files_1[index % len(self.files_1)]), seconds = self.seconds)
            item_2 = utils.signal_to_spec(utils.load_wav(self.files_2[offset_index % len(self.files_2)]), seconds = self.seconds)
        return {self.d1: item_1, self.d2: item_2}

    def __len__(self):
        num_files = max(len(self.files_1), len(self.files_2))
        if self.data_prop < 1:
            num_files = int(num_files * self.data_prop)
        return num_files



class SongTestDataset(Dataset):
    def __init__(self, root, data_prop = 0.05, db = True, d1 = 'pop', d2 = 'rock'):

        self.d1 = d1
        self.d2 = d2

        self.files_1 = sorted(glob.glob(os.path.join(root, self.d1) + '/*.*'))
        self.files_2 = sorted(glob.glob(os.path.join(root, self.d2) + '/*.*'))

        self.data_prop = data_prop
        self.db = db

    def __getitem__(self, index):
        if self.db:
            item_1, _, _ = utils.signal_to_spec_full(utils.load_wav(self.files_1[index % len(self.files_1)]))
            item_2, _, _ = utils.signal_to_spec_full(utils.load_wav(self.files_2[index % len(self.files_2)]))
        else:
            item_1, _, _ = utils.signal_to_spec_full(utils.load_wav(self.files_1[index % len(self.files_1)]))
            item_2, _, _ = utils.signal_to_spec_full(utils.load_wav(self.files_2[index % len(self.files_2)]))

        return {self.d1: item_1, self.d2: item_2}

    def __len__(self):
        num_files = max(len(self.files_1), len(self.files_2))
        if self.data_prop < 1:
            num_files = int(num_files * self.data_prop)
        return num_files

# Return songs from either pop or rock in pairs with the mel spectrogram
class SignalDataset(Dataset):
    def __init__(self, root, data_prop = 1, db = True, quantization = 256):
        self.files = sorted(glob.glob(os.path.join(root, 'pop') + '/*.*')) + sorted(glob.glob(os.path.join(root, 'rock') + '/*.*'))
        self.data_prop = data_prop
        self.quant = quantization
        self.db = db

    def __len__(self):
        n_files = len(self.files)
        if self.data_prop < 1:
            n_files = int(n_files * self.data_prop)
        return n_files

    def __getitem__(self, index):
        if self.db:
            signal = utils.load_wav(self.files[index])
            spec = utils.signal_to_spec_db(signal)
        else:
            signal = utils.load_wav(self.files[index])
            spec = utils.signal_to_spec(signal)
        
        # Quantize signal
        qsignal = ((signal - signal.min()) / (signal.max() - signal.min()) * (self.quant - 1)).astype(int)
        
        qsignal = utils.signal_window(qsignal)
        return {'signal':qsignal, 'spec':spec}